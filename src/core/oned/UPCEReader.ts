/*
 * Copyright 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import BarcodeFormat from '../BarcodeFormat';
import FormatException from '../FormatException';
import BitArray from '../common/BitArray';

import UPCEANReader from './UPCEANReader';
import NotFoundException from '../NotFoundException';

/**
 * <p>Implements decoding of the EAN-13 format.</p>
 *
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen
 * @author alasdair@google.com (Alasdair Mackintosh)
 */
export default class UPCEReader extends UPCEANReader {
    private static MIDDLE_END_PATTERN: number[] = [1, 1, 1, 1, 1, 1];
    private static NUMSYS_AND_CHECK_DIGIT_PATTERNS: number[][] = [[0x38, 0x34, 0x32, 0x31, 0x2C, 0x26, 0x23, 0x2A, 0x29, 0x25], [0x07, 0x0B, 0x0D, 0x0E, 0x13, 0x19, 0x1C, 0x15, 0x16, 0x1A]]
    private decodeMiddleCounters: number[];

    public constructor() {
        super();
        this.decodeMiddleCounters = [0, 0, 0, 0];
    }

    public decodeMiddle(row: BitArray, startRange: number[], resultString: string) {
        let counters = this.decodeMiddleCounters;
        counters[0] = 0;
        counters[1] = 0;
        counters[2] = 0;
        counters[3] = 0;
        let end = row.getSize();
        let rowOffset = startRange[1];

        let lgPatternFound = 0;

        for (let x = 0; x < 6 && rowOffset < end; x++) {
            let bestMatch = UPCEANReader.decodeDigit(row, counters, rowOffset, UPCEANReader.L_AND_G_PATTERNS);
            resultString += String.fromCharCode(('0'.charCodeAt(0) + bestMatch % 10));
            for (let counter of counters) {
                rowOffset += counter;
            }
            if (bestMatch >= 10) {
                lgPatternFound |= 1 << (5 - x);
            }
        }

        resultString = UPCEReader.determineNumSysAndCheckDigit(resultString, lgPatternFound);

        return { rowOffset, resultString };
    }

    public getBarcodeFormat(): BarcodeFormat {
        return BarcodeFormat.UPC_E;
    }

    public decodeEnd(row: BitArray, endStart: number) {
        return UPCEANReader.findGuardPattern(row, endStart, true, UPCEReader.MIDDLE_END_PATTERN, new Array(UPCEReader.MIDDLE_END_PATTERN.length).fill(0));
    }

    public checkChecksum(s: string): boolean {
        return UPCEANReader.checkChecksum(UPCEReader.convertUPCEtoUPCA(s));
    }

    static determineNumSysAndCheckDigit(resultString: string, lgPatternFound: number) {
        for (let numSys = 0; numSys <= 1; numSys++) {
            for (let d = 0; d < 10; d++) {
                if (lgPatternFound === this.NUMSYS_AND_CHECK_DIGIT_PATTERNS[numSys][d]) {
                    resultString = String.fromCharCode(('0'.charCodeAt(0) + numSys)) + resultString;
                    resultString = resultString + String.fromCharCode(('0'.charCodeAt(0) + d));
                    return resultString;
                }
            }
        }
        throw new NotFoundException();
    }

    static convertUPCEtoUPCA(upce: string): string {
        let upceChars: string = upce.substring(1, 7);
        var result: string = upce.charAt(0);
        let lastChar: string = upceChars.charAt(5);
        switch (lastChar) {
            case '0':
            case '1':
            case '2':
                result = result + upceChars.substring(0, 2);
                result = result + lastChar;
                result = result + "0000";
                result = result + upceChars.substring(2, 5);
                break;
            case '3':
                result = result + upceChars.substring(0, 3);
                result = result + "00000";
                result = result + upceChars.substring(3, 5);
                break;
            case '4':
                result = result + upceChars.substring(0, 4);
                result = result + "00000";
                result = result + upceChars.charAt(4);
                break;
            default:
                result = result + upceChars.substring(0, 5);
                result = result + "0000";
                result = result + lastChar;
                break;
        }
        // Only append check digit in conversion if supplied
        if (upce.length >= 8) {
            result = result + upce.charAt(7);
        }
        return result;
    }
}
